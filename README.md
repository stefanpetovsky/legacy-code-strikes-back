# When legacy code strikes back!

## Prvá pomoc, keď sa darí

- [Opravár vs. LaKaToš - zostrih]
- [Opravár vs. LaKaToš - zostrih 2]
- ["Jebem ja tvojho boha"]

[Opravár vs. LaKaToš - zostrih]: http://vyhlasto.cz/h/3a1/abych-mohl-toto
[Opravár vs. LaKaToš - zostrih 2]: http://nejen.cz/lakatos/
["Jebem ja tvojho boha"]: https://www.youtube.com/watch?v=qCHeJfpeOH8

## Meliško
- [Cibuľa]
- [Bubny]
- [Mravce]
- [Voda]

[Cibuľa]: https://www.youtube.com/watch?v=GhcTFaKhagI
[Bubny]: https://www.youtube.com/watch?v=VgSQ2fMwGKc
[Mravce]: https://www.youtube.com/watch?v=p96Ig2ip2kE
[Voda]: https://www.youtube.com/watch?v=p7C8gjfgkkk

## Perly slovenského internetu

- [Oprava slovenského Lakatoša]
- [Kapučínko] 
- [Sme narafali]
- [Motorkár z Mníchovej Lehoty]
- [Cigáň volá na políciu]
- [Viete ako...aby ma Aďka mala rada]
- [Omietkový démon]
- [Mydlil barana]
- [Alkotester]
- [Čo je veľo to je málo] alias [Nech prepáči slovenská únia]
- [Star Wars]
- [Generátor náhodných slov]
- [Norbert Lakatoš hľadá dievča]
- [Dve pesá]

[Oprava slovenského Lakatoša]: https://www.youtube.com/watch?v=SiUz_akTmcY
[Kapučínko]: https://www.youtube.com/watch?v=AQPnZwl-Pn8
[Sme narafali]: https://www.youtube.com/watch?v=cOelmiCl1j8
[Motorkár z Mníchovej lehoty]: https://www.youtube.com/watch?v=t0CXcckl8I4
[Cigáň volá na políciu]: https://www.youtube.com/watch?v=5L9cXh8k-Dw
[Viete ako...aby ma Aďka mala rada]: https://www.youtube.com/watch?v=Twv3bEFddNM
[Omietkový démon]: https://www.youtube.com/watch?v=Aev662LXeXc
[Mydlil barana]: https://www.youtube.com/watch?v=LAkdPXJqkC0
[Alkotester]: https://www.youtube.com/watch?v=Drf33wdvvbE
[Čo je veľo to je málo]: https://www.youtube.com/watch?v=L6SCrXZ_uTg
[Nech prepáči slovenská únia]: http://www.mojevideo.sk/video/24885/co_je_vela_to_je_malo_(generator_nahodnych_slov).html
[Star Wars]: https://www.youtube.com/watch?v=86KVHTT-m_s
[Generátor náhodných slov]: https://www.youtube.com/watch?v=T6HpKJbju9k
[Norbert Lakatoš hľadá dievča]: https://www.youtube.com/watch?v=wYt7CH43SlU
[Dve pesá]: https://www.youtube.com/watch?v=dYW7tYEODYw
